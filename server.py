from flask import Flask, json, Response
import subprocess

app = Flask(__name__)


@app.route('/status')
def status():
    rawIp = subprocess.check_output('dig @resolver1.opendns.com ANY myip.opendns.com +short', stderr=subprocess.STDOUT, shell=True)
    ip = rawIp.decode("utf-8")

    rawHostname = subprocess.check_output('hostname', stderr=subprocess.STDOUT, shell=True)
    hostname = rawHostname.decode("utf-8")

    rawCPU = subprocess.check_output('cat /proc/cpuinfo | grep \'cpu cores\' | awk \'{print $4}\'', stderr=subprocess.STDOUT, shell=True)
    CPU = rawCPU.decode("utf-8")

    rawMemory = subprocess.check_output('awk \'/MemTotal/ { printf "%.3f \n", $2/1024/1024 }\' /proc/meminfo', stderr=subprocess.STDOUT, shell=True)
    memory = rawMemory.decode("utf-8")

    data = {
        'hostname': hostname.replace('\n', ''),
        'ip_address': ip.replace('\n', ''),
        'cpus': CPU.replace('\n', ''),
        'memory': memory.replace('\n', '')
    }

    js = json.dumps(data)

    resp = Response(js, status=200, mimetype='application/json')

    return resp


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80)
